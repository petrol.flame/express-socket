import Env from "./App/Config/Env.js";
import { createServer } from "http";
import express from "express";
import { Server } from "socket.io";
import { roomConnection } from "./rooms.js";
import { authController } from "./App/Controllers/authController.js";
import { authenticate } from './App/Middlewares/authMiddleware.js';
import swaggerUiExpress from 'swagger-ui-express';
import swaggerData from "./swagger.json" assert {type: 'json'};



const app = express();

const httpServer = createServer(app);

const io = new Server(httpServer);

app.use(express.json());
const options = {
    customCss: '.swagger-ui .models { display: none }'
  };  
app.use('/api-docs',swaggerUiExpress.serve,swaggerUiExpress.setup(swaggerData,options));

io.on('connection', (socket) => {

    socket.on('chatWith', (sender, reciever) => {
        let getChatOBJ = roomConnection(sender, reciever);
        socket.join(getChatOBJ.room_id);
        socket.emit('getChatObj', getChatOBJ);
    })

    socket.on('sendMessage', (data) => {
        io.in(data.room_id).emit('recieveMessage', data);
    })

});


/**
 * Routes
 */

app.post('/register', authController.register);
app.post('/login', authController.login);

app.use('/auth',authenticate);
app.get('/auth/user',(req,res) => {
    return res.send(req.user);
})
app.get('/auth/users',authController.getUsers);


httpServer.listen(process.env.PORT);
console.log('\x1b[32m%s\x1b[0m', `Listening on port ${process.env.PORT}`);