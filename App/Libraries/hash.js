import * as bcrypt from "bcrypt";

const saltRounds = 10;

export const hasher = async (code) => {
    return bcrypt.hash(code,saltRounds)
}

export const checkHash = async (passcode, hash) => {
    return bcrypt.compare(passcode, hash);
}