import jsonwebtoken from "jsonwebtoken";;
import { create, getUserbyEmail, gettingUsers } from "../Models/User.js";
import { loginRules, registerRules, unique } from "../Validations/UserRules.js";
import { checkHash } from "../Libraries/hash.js";

export class authController {

    static async register(req, res) {
        try {
            const validate = registerRules.validate(req.body);
            if (validate.error && validate.error.details) {
                return authController.sendErrors(validate, res);
            }
            if (await unique(req)) {
                return res.status(422).send({ username: "username or email already exists" });
            }
            let data = await create(req);
            let response = {_id:data._id,username:data.username,email:data.email};
            let token = authController.generateToken(response);
            response.token = token
            return res.send(response);
        } catch (error) {
            return res.status(400).send(error.stack);
        }
    }

    static async login(req, res) {
        try {
            const validate = loginRules.validate(req.body);
            if (validate.error && validate.error.details) {
                return authController.sendErrors(validate, res);
            }
            let user = await getUserbyEmail(req);
            if (!user) {
                return res.status(422).send({ email: "invalid email" });
            }
            let checkPass = await checkHash(req.body.password,user.password);
            if(!checkPass){
                return res.status(400).send({ password: "incorrect password" });
            }
            const token = authController.generateToken({_id:user._id,username:user.username,email:user.email});
            const authUser = {
                _id:user._id,
                username:user.username,
                email:user.email,
                token:token,
            }
            return res.send(authUser);
        } catch (error) {
            return res.status(400).send(error.stack);
        }
    }

    static sendErrors(validate, res) {
        let errors = {};
        validate.error.details.forEach((e) => {
            errors[e.context.key] = e.message;
        });
        return res.status(422).send(errors);
    }

    static generateToken(data) {
        return jsonwebtoken.sign(data, process.env.JWT_SECRET, { expiresIn: "2h" });
    }

    static async getUsers(req,res) {
        try {
            const users = await gettingUsers(req);
            return res.send(users) 
        } catch (error) {
            return res.status(400).send(error.stack)
        }
    }
}