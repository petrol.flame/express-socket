import Joi from "joi";
import { User } from "../Models/User.js";

export const unique = async (req) => {
    let name = await User.findOne({ "username": req.body.username });
    let mail = await User.findOne({ "email": req.body.email });
    if (name || mail) {
        return true;
    }
    return false;
}

export const registerRules = Joi.object({
    username: Joi.string()
        .required(),

    email: Joi.string()
        .required()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),

    password: Joi.string()
        .required(),
}).options({
    abortEarly: false,
});

export const loginRules = Joi.object({
    email: Joi.string()
        .required()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),

    password: Joi.string()
        .required(),
}).options({
    abortEarly: false,
});


