import { faker } from '@faker-js/faker';

export class UserFactory {
    run() {
        return {
            username: faker.internet.userName(),
            email: faker.internet.email(),
            password: faker.internet.password()
        };
    }

    static make(count){
        const USERS = [];
        Array.from({ length: count }).forEach(() => {
            USERS.push(this.prototype.run());
        });
        return USERS;
    }

}


