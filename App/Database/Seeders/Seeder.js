import Env from "../../Config/Env.js";
import { UserSeeder } from "./UserSeeder.js"

/**
 * Execute all seeds
 */
export const seeding = async () => {
    await UserSeeder.run();
    console.log("seeding done");
    process.exit()
}
