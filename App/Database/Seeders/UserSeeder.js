import { User } from "../../Models/User.js";
import { UserFactory } from "../Factories/UserFactory.js";

export class UserSeeder {

    static async run(){
        const count = await User.count()
        if (count < 15) {
            const records = UserFactory.make(10);
            await User.insertMany(records);
        }
    }
}