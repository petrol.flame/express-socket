export class UserListing {
    /**
     *User listing pipeline for mongo aggregation 
     */
    static pipeline(req){
        const limit = parseInt((req.query.limit && req.query.limit > 0) ? req.query.limit : '10');
        const skip = parseInt(req.query.skip ? req.query.skip : '0');
        return [
            {
                '$match': {
                    'email': {
                        '$not': {
                            '$eq': req.user.email
                        }
                    }
                }
            },
            {
                '$project': {
                    '_id': true,
                    'username': true,
                    'email': true,
                    'createdAt': true,
                    'updatedAt': true
                }
            }
            , {
                '$facet': {
                    'data': [
                        {
                            '$skip': skip
                        }, {
                            '$limit': limit
                        }
                    ],
                    'count': [
                        {
                            '$count': 'count'
                        }
                    ]
                }
            }, {
                '$addFields': {
                    'count': {
                        '$arrayElemAt': [
                            '$count', 0
                        ]
                    },
                    'skipped': skip,
                    'perPage': limit,
                }
            }, {
                '$project': {
                    'data': '$data',
                    'total': '$count.count',
                    'skipped': '$skipped',
                    'perPage': '$perPage',
                }
            }
        ]
    }
}













