import { Database } from "../Config/Database.js";
import { hasher } from "../Libraries/hash.js";
import { UserListing } from "./Aggregations/UserListing.js";

const DB = Database.getInstance();
const mongo = DB.getMongoInstance();

const Schema = mongo.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        unique: true
    },
    email: {
        type: String,
        unique: true
    },
    password: {
        type: String,
        select: false
    },
}, { timestamps: true });

export const User = mongo.model('users', UserSchema);

export async function create(req) {
    req.body.password = await hasher(req.body.password)
    let saveRecord = new User(req.body);
    return await saveRecord.save();
}

export async function getUserbyEmail(req) {
    let user = await User.findOne({ "email": req.body.email }).select('+password');
    return user;
}

export async function gettingUsers(req) {
    let users = await User.aggregate(UserListing.pipeline(req));
    return users[0];
}

