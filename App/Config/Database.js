import mongoose from "mongoose";

export class Database {
    instance;

    static getInstance() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new Database();
        return this.instance;
    }

    getMongoInstance() {
        try {            
            mongoose.connect(process.env.DB_CONNECT)
            .catch((err) => {
                console.log("\x1b[31m",'[ERROR] mongo '+err.message);
                process.exit(1)
            })
            let db = mongoose.connection;
            
            db.once("open", function () {
                console.log('\x1b[32m%s\x1b[0m',"Mongodb connected");
            });
            return mongoose;
        } catch (error) {
            console.log(error.message);
        }
    }
}
