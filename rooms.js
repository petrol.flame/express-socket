import sha256 from 'sha-256-js';

let rooms = [];

export const roomConnection = (user1, user2) => {
    let data = {
        sender: user1,
        reciever: user2,
        roomId: sha256(user1 + user2),
    }
    let ID = null;
    let findRoom1 = rooms.find(e => e.reciever == user1 && e.sender == user2);
    let findRoom2 = rooms.find(e => e.sender == user1 && e.reciever == user2);
    if(!findRoom1 && !findRoom2){
        rooms.push(data);
        ID = data.roomId;
    }
    if(findRoom1 || findRoom2){
        ID = findRoom1 ? findRoom1.roomId : findRoom2.roomId;
    }
    return {room_id:ID, chatBetween: findRoom1 ? findRoom1 : (findRoom2 ? findRoom2 : data)};
}